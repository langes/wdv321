// Form Validation Functions


// Function to check for a valid email address
function validateEmail(inVal)
{
	var emailRegex = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})$/);
	return emailRegex.test(inVal);
}
	
// Function to check for a valid phone number.
function validatePhoneNumber(inVal)
{
	var phoneNumberRegex = new RegExp(/^[1-9]\d{2}-\d{3}-\d{4}$/);
	return phoneNumberRegex.test(inVal);
}

// Function to check for a valid name, with no special characters or numbers.
function validateName(inVal)
{
	var nameRegex = new RegExp(/^([a-zA-Z]{2,16})$/);
	return nameRegex.test(inVal);
}

// Function to check for valid textarea input, without the following characters: < > '
function validateTextArea(inVal)
{
	var textAreaRegex = new RegExp(/('|\<|\>)+/gi); 
	return !(textAreaRegex.test(inVal));
}