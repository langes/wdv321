//Cookie functions for Track Recipe Project & Dynamic Recipe Project

//Updates the views of a recipe.
function recipeViewsCounter(inRecipe)
{
	var recipeViews = getCookie(inRecipe);
	
	if(!recipeViews)
	{
		recipeViews = 1;
	}
	else
	{
		recipeViews = parseInt(recipeViews) + 1;
	}
	setCookie(inRecipe, recipeViews);
}
//Sets a cookie with a 1 year expiration date
var viewStartTime = ""; //To hold current recipe's view start time.
function setCookie(cookieName, cookieValue) 
{
	var exDate = new Date();
	viewStartTime = exDate.getTime();
	exDate.setTime(exDate.getTime() + (365*24*60*60*1000));
	var cExpires = "expires=" + exDate.toUTCString();
	document.cookie = cookieName + "=" + cookieValue + ";" + cExpires + ";path=/";
}
//Gets the value of a cookie.
function getCookie(cookieName)
{
	var cookieValue = null;
	var findCookie = document.cookie + ";";
	var findName = cookieName + "=";
	var endPos;
	if (findCookie.length > 0 ) 
	{
		var beginPos = findCookie.indexOf(findName);
		if (beginPos != -1) 
		{
			beginPos = beginPos + findName.length;
			endPos = findCookie.indexOf(";", beginPos);
			if (endPos == -1)
				endPos = findCookie.length;
			cookieValue = findCookie.substring(beginPos, endPos);
		}
	} 
   return cookieValue;   
}
//Gets & sets time spent on a recipe.
function recipeTime(inRecipe) 
{
	//Determines time spent on a recipe.
	var viewEndTime = new Date().getTime();
	var elapsedTime = viewEndTime - viewStartTime;
	
	//Checks to see if a time cookie exists.
	var cookieTimeName = inRecipe + "AvgViews";
	var cookieTime = getCookie(cookieTimeName); //Gets total elapsed time
	if(!cookieTime) //If cookie does not exist, sets cookieTime to current elapsedTime.
	{
		cookieTime = parseInt(elapsedTime);
	}
	else //If cookie exists, updates cookieTime value with the elapsedTime.
	{
		cookieTime = parseInt(cookieTime) + parseInt(elapsedTime);
	}
	setCookie(cookieTimeName, cookieTime);
}
//Determines average time spent on a recipe.
function getAvgTime(inCookieName)
{
	var getViews = getCookie(inCookieName); //Get cookie views.
	var getTotalTime = getCookie(inCookieName + "AvgViews"); //Get cookie recipe time.
	var avgViewTime = parseInt(getTotalTime) / parseInt(getViews); //Calculate average time.

	if(avgViewTime > 0)
	{
		var seconds = Math.round(avgViewTime / 1000);
		var minutes = Math.round(seconds / 60);
		var hours = Math.round(minutes / 60);
		var sec = TrimSecondsMinutes(seconds);
		var min = TrimSecondsMinutes(minutes);
		var viewTime = hours + " hrs " + min + " mins " + sec + " sec";
		return viewTime;
	}
}
function TrimSecondsMinutes(inTime)
{
    if (inTime >= 60)
        return TrimSecondsMinutes(inTime - 60);
    return inTime;
}